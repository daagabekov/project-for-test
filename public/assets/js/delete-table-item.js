let url = "";

$(document).on("click", ".show-delete-item-modal", function () {
    url = $(this).data("url");
});

$(document).on("click", ".delete-item", function () {
    axios
        .delete(url)
        .then(function (response) {
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
        });
});
