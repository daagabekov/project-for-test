@push('styles')
    <style>
        .dd-option-image,
        .dd-selected-image {
            width: 2em;
        }
    </style>
@endpush

<x-admin-panel.layouts.app>
    <div class="row">
        <div class="col-xxl-12">

            <x-admin-panel.layouts.error-list />

            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">{{ $form_title }}</h4>
                </div>
                <div class="card-body">
                    <div class="live-preview">
                        <form action="{{ $action_url }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @isset($product)
                                @method('PUT')
                            @endisset
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <x-admin-panel.forms.text-input name="title" type="text" label="Название"
                                            value="{{ $product->title ?? null }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <x-admin-panel.forms.text-input name="price" type="number"
                                            label="Цена" value="{{ $product->price ?? null }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <x-admin-panel.forms.textarea name="description" label="Описание">
                                            {{ $product->description ?? null }}
                                        </x-admin-panel.forms.textarea>
                                    </div>
                                </div>

                                <div class="text-end">
                                    <a href="{{ route('product.index') }}" class="btn btn-danger">Назад</a>
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div>
        <!--end col-->
    </div>
</x-admin-panel.layouts.app>
