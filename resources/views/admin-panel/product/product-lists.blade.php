@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
@endpush

@push('scripts')
    <!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                "order": [],
                "language": {
                    url: '//cdn.datatables.net/plug-ins/1.13.4/i18n/ru.json',
                },
                "columnDefs": [{
                        "width": "15%",
                        "orderable": false,
                        "targets": -1
                    },
                    {
                        "width": "5%",
                        "targets": 0
                    }
                ]
            });
        });
    </script>
@endpush

<x-admin-panel.layouts.app>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h5 class="card-title mb-0 flex-grow-1">Продукты</h5>
                    <div>
                        <a href="{{ route('product.create') }}" class="btn btn-primary">Добавить</a>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle"
                        style="width:100%">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>
                                        <a href="{{ route('product.edit', ['product' => $item->id]) }}"
                                            class="btn btn-sm btn-soft-info">Редактировать</a>
                                        <button class="btn btn-sm btn-soft-danger show-delete-item-modal"
                                            data-bs-toggle="modal" data-bs-target=".delete-modal"
                                            data-url="{{ route('product.destroy', ['product' => $item->id]) }}">Удалить</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--end col-->
    </div>
    <!--end row-->
</x-admin-panel.layouts.app>
