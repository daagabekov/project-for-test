@props([
    'multiple' => false,
    'required' => false,
    'accept' => null,
    'name',
    'label',
])

<div>
    <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    <input type="file" class="form-control" id="{{ $name }}" name="{{ $name }}"
        accept="{{ $accept }}" {{ $multiple ? 'multiple' : '' }} {{ $required ? 'required' : '' }}>
</div>
