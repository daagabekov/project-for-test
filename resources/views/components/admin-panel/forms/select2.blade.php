@props([
    'multiple' => false,
    'required' => false,
    'search' => true,
    'value' => [],
    'label' => '',
    'name',
    'items',
    'class',
])

@pushOnce('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endPushOnce

<div>

    @if (!empty($label))
        <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    @endif

    <select class="select2 {{ $class ?? '' }}" name="{{ $name }}" {{ $multiple ? 'multiple' : '' }}
        {{ $required ? 'required' : '' }}>
        <option value="">{{ $label }}</option>
        @foreach ($items as $key => $item)
            <option value="{{ $key }}" {{ in_array($key, $value) ? 'selected' : '' }}>
                {{ $item }}
            </option>
        @endforeach
    </select>
</div>

@pushOnce('scripts')
    <!--select2 cdn-->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endPushOnce

@push('scripts')
    <script>
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
@endpush
