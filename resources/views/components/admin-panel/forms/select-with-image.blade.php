@props([
    'required' => false,
    'value' => [],
    'label' => '',
    'name',
    'items',
    'id',
])

@pushOnce('scripts')
    <script src="https://cdn.rawgit.com/prashantchaudhary/ddslick/master/jquery.ddslick.min.js"></script>
@endPushOnce

@push('scripts')
    <script>
        $('#{{ $id }}').ddslick();
    </script>
@endpush

@pushOnce('styles')
    <style>

    </style>
@endPushOnce

<div>

    @if (!empty($label))
        <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    @endif

    <select id="{{ $id }}" name="{{ $name }}" {{ $required ? 'required' : '' }} class="form-control">
        <option value="" data-imagesrc="" data-description="">{{ $label }}</option>
        @foreach ($items as $key => $item)
            <option value="{{ $key }}" data-imagesrc="{{ $item['image'] }}"
                data-description="{{ $item['sub_title'] }}" {{ in_array($key, $value) ? 'selected' : '' }}>
                {{ $item['title'] }}
            </option>
        @endforeach
    </select>
</div>
