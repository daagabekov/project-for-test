@props([
    'rows' => null,
    'cols' => null,
    'placeholder' => null,
    'required' => false,
    'label' => null,
    'name',
])

<div>
    @if ($label)
        <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    @endif

    <textarea class="form-control" id="{{ $name }}" name="{{ $name }}" rows="{{ $rows }}"
        cols="{{ $cols }}" placeholder="{{ $placeholder }}" {{ $required ? 'required' : '' }}>{{ $slot }}</textarea>
</div>
