@pushOnce('styles')
    <link href="{{ asset('assets/libs/quill/quill.core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/quill/quill.bubble.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/quill/quill.snow.css') }}" rel="stylesheet" type="text/css" />
@endPushOnce

<div class="snow-editor" style="height: 300px;">
    {!! html_entity_decode($value) !!}
</div>
<input type="hidden" name="{{ $name }}" id="{{ $name }}">

@pushOnce('scripts')
    <script src="{{ asset('assets/libs/quill/quill.min.js') }}"></script>
    <script>
        var quill = new Quill('.snow-editor', {
            modules: {
                toolbar: [
                    [{
                        font: []
                    }, {
                        size: []
                    }],
                    ["bold", "italic", "underline", "strike"],
                    [{
                        color: []
                    }, {
                        background: []
                    }],
                    [{
                        script: "super"
                    }, {
                        script: "sub"
                    }],
                    [{
                        header: [!1, 1, 2, 3, 4, 5, 6]
                    }, "blockquote", "code-block"],
                    [{
                            list: "ordered"
                        },
                        {
                            list: "bullet"
                        },
                        {
                            indent: "-1"
                        },
                        {
                            indent: "+1"
                        },
                    ],
                    ["direction", {
                        align: []
                    }],
                    ["link", "image"],
                    ["clean"],
                ],
            },
            theme: 'snow'
        });

        quill.on('text-change', function() {
            var html = quill.root.innerHTML;
            $("#{{ $name }}").val(html)
        });
    </script>
@endPushOnce
