@props([
    'type' => 'text',
    'placeholder' => null,
    'value' => null,
    'required' => false,
    'label' => null,
    'mask' => null,
    'name',
])

@pushOnce('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
@endPushOnce

@pushIf($mask, 'scripts')
<script>
    $(document).ready(function() {
        $('#{{ $name }}').mask(@json($mask));
    });
</script>
@endPushIf

<div>
    @if ($label)
        <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    @endif

    <input type="{{ $type }}" class="form-control" id="{{ $name }}" name="{{ $name }}"
        placeholder="{{ $placeholder }}" value="{{ $value }}" {{ $required ? 'required' : '' }}>
</div>
