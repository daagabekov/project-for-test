@pushOnce('styles')
    <style>
        .ck-powered-by {
            display: none;
        }
    </style>
@endPushOnce

<div>
    @if (!empty($label))
        <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    @endif
    <textarea class="ckeditor" name="{{ $name }}" id="{{ $name }}">{{ $slot }}</textarea>
</div>

@pushOnce('scripts')
    <script src="{{ asset('assets/ckeditor-5/build/ckeditor.js') }}"></script>
    <script src="/assets/ckeditor-5/build/translations/{{ app()->getLocale() }}.js"></script>
@endPushOnce

@push('scripts')
    <script>
        ClassicEditor
            .create(document.querySelector('#{{ $name }}'), {
                language: @json(app()->getLocale()),
                cloudServices: {
                    tokenUrl: ''
                }
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endpush
