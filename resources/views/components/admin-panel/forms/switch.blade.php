@props([
    'required' => false,
    'checked' => false,
    'name',
    'label',
    'class',
])

<div class="form-check form-switch form-switch-lg" dir="ltr">
    <input type="hidden" value="{{ isset($checked) && $checked ? '1' : '0' }}" name="{{ $name }}">
    <input type="checkbox" class="form-check-input" id="{{ $name }}" {{ $checked ? 'checked' : '' }}
        value="{{ $checked ? '1' : '0' }}" {{ $required ? 'required' : '' }}>
    <label class="form-check-label" for="{{ $name }}">{{ $label }}</label>
</div>

@push('scripts')
    <script>
        $('.form-switch .form-check-input').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).val(1);
                $(this).attr('checked', true);
                $("input[name='{{ $name }}'][type='hidden']").val(1);
            } else {
                $(this).val(0);
                $(this).attr('checked', false);
                $("input[name='{{ $name }}'][type='hidden']").val(0);
            }
        })
    </script>
@endpush
