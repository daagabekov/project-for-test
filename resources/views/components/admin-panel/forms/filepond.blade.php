@props([
    'multiple' => false,
    'required' => false,
    'accept' => null,
    'value' => null,
    'file_data' => null,
    'name',
    'id',
])

@pushOnce('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/filepond/filepond.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/libs/filepond-plugin-file-poster/filepond-plugin-file-poster.css') }}"
        type="text/css" />
    <link rel="stylesheet"
        href="{{ asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.css') }}">
@endPushOnce

<div>
    <input type="file" class="filepond" id="{{ $id }}" name="{{ $name }}"
        accept="{{ $accept }}" {{ $multiple ? 'multiple' : '' }} {{ $required ? 'required' : '' }}>
</div>

@push('scripts')
    <script src="{{ asset('assets/libs/filepond/filepond.min.js') }}"></script>
    <script src="{{ asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.js') }}"></script>
    <script src="{{ asset('assets/libs/filepond-plugin-file-poster/filepond-plugin-file-poster.js') }}"></script>
    <script src="{{ asset('assets/libs/filepond-plugin-file-validate-size/filepond-plugin-file-validate-size.min.js') }}">
    </script>
    <script
        src="{{ asset('assets/libs/filepond-plugin-image-exif-orientation/filepond-plugin-image-exif-orientation.min.js') }}">
    </script>
    <script src="{{ asset('assets/libs/filepond-plugin-file-encode/filepond-plugin-file-encode.min.js') }}"></script>
    <script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginImageExifOrientation,
            FilePondPluginFileValidateSize,
            FilePondPluginFilePoster,
            // FilePondPluginFileEncode,
        );
    </script>

    @if ($value)
        @if (is_array($value))
            <script>
                FilePond.create(document.querySelector("#{{ $id }}"), {
                    storeAsFile: true,
                    files: [
                        @foreach ($value as $val)
                            {
                                source: @json($val),
                            },
                        @endforeach
                    ],
                });
            </script>
        @else
            <script>
                FilePond.create(document.querySelector("#{{ $id }}"), {
                    storeAsFile: true,
                    files: [{
                        source: @json($value),
                    }, ],
                });
            </script>
        @endif
    @else
        <script>
            FilePond.create(document.querySelector("#{{ $id }}"), {
                storeAsFile: true,
            });
        </script>
    @endif

@endpush
