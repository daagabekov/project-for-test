@props([
    'label' => '',
    'name',
    'value',
])

@pushOnce('styles')
    <style>
        .choices__list {
            z-index: 4 !important;
        }
    </style>
@endPushOnce

<div>

    @if (!empty($label))
        <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    @endif

    <select class="form-control" id="{{ $name }}" name="{{ $name }}">
        <option value="1" {{ $value == 1 ? 'selected' : '' }}>
            Да
        </option>
        <option value="0" {{ $value == 0 ? 'selected' : '' }}>
            Нет
        </option>
    </select>
</div>
