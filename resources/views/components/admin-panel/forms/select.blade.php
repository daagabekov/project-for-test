@props([
    'multiple' => false,
    'required' => false,
    'search' => true,
    'value' => [],
    'label' => '',
    'name',
    'id',
    'items',
    'class',
])

@pushOnce('styles')
    <style>
        .choices__list {
            z-index: 4 !important;
        }
    </style>
@endPushOnce

<div>

    @if (!empty($label))
        <label for="{{ $id }}" class="form-label">{{ $label }}</label>
    @endif

    <select class="form-control" id="{{ $id }}" name="{{ $name }}" data-choices data-choices-removeItem
        {{ $search ? '' : 'data-choices-search-false' }} {{ $multiple ? 'multiple' : '' }}
        {{ $required ? 'required' : '' }}>
        <option value="">{{ $label }}</option>
        @foreach ($items as $key => $item)
            <option value="{{ $key }}" {{ in_array($key, $value, true) ? 'selected' : '' }}>
                {{ $item }}
            </option>
        @endforeach
    </select>
</div>
