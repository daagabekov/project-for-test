@props([
    'required' => false,
    'checked' => false,
    'name',
    'label',
    'value',
    'color',
    'id',
])

<div>
    <input type="radio" class="btn-check" name="{{ $name }}" id="{{ $id }}" value="{{ $value }}"
        {{ $checked ? 'checked' : '' }} {{ $required ? 'required' : '' }}>
    <label class="btn btn-outline-{{ $color }}" for="{{ $id }}">{{ $label }}</label>
</div>

@push('scripts')
@endpush
