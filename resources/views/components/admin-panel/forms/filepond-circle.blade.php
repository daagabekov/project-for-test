@props([
    'required' => false,
    'accept' => null,
    'value' => null,
    'name',
])

@pushOnce('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/filepond/filepond.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/libs/filepond-plugin-file-poster/filepond-plugin-file-poster.css') }}"
        type="text/css" />
    <link rel="stylesheet"
        href="{{ asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.css') }}">
@endPushOnce

<div class="avatar-xl mx-auto">
    <input type="file" class="filepond filepond-input-circle" id="{{ $name }}" name="{{ $name }}"
        accept="{{ $accept }}" {{ $required ? 'required' : '' }}>
</div>

@push('scripts')
    <script src="{{ asset('assets/libs/filepond/filepond.min.js') }}"></script>
    <script src="{{ asset('assets/libs/filepond-plugin-image-preview/filepond-plugin-image-preview.min.js') }}"></script>
    <script src="{{ asset('assets/libs/filepond-plugin-file-poster/filepond-plugin-file-poster.js') }}"></script>
    <script src="{{ asset('assets/libs/filepond-plugin-file-validate-size/filepond-plugin-file-validate-size.min.js') }}">
    </script>
    <script
        src="{{ asset('assets/libs/filepond-plugin-image-exif-orientation/filepond-plugin-image-exif-orientation.min.js') }}">
    </script>
    <script src="{{ asset('assets/libs/filepond-plugin-file-encode/filepond-plugin-file-encode.min.js') }}"></script>
    <script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginImageExifOrientation,
            FilePondPluginFileValidateSize,
            FilePondPluginFilePoster,
            // FilePondPluginFileEncode,
        );

        let options = {
            storeAsFile: true,
            imagePreviewHeight: 170,
            imageCropAspectRatio: "1:1",
            imageResizeTargetWidth: 200,
            imageResizeTargetHeight: 200,
            stylePanelLayout: "compact circle",
            styleLoadIndicatorPosition: "center bottom",
            styleProgressIndicatorPosition: "right bottom",
            styleButtonRemoveItemPosition: "left bottom",
            styleButtonProcessItemPosition: "right bottom",
        }
    </script>

    @if ($value)
        <script>
            options.files = [{
                source: @json($value),
            }];
        </script>
    @endif

    <script>
        FilePond.create(document.querySelector("#{{ $name }}"), options);
    </script>
@endpush
