<div class="mt-3">
    <lord-icon src="https://cdn.lordicon.com/zthozvfn.json" trigger="loop"
        colors="primary:#b26836,secondary:#4bb3fd,tertiary:#f9c9c0,quaternary:#ebe6ef" style="width:120px;height:120px">
    </lord-icon>
    <div class="mt-4 pt-2 fs-15">
        <h4>Назначение выполнено!</h4>
    </div>
</div>
