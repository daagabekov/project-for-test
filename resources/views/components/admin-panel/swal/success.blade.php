<div class="mt-3">
    <lord-icon src="https://cdn.lordicon.com/lupuorrc.json" trigger="loop" colors="primary:#0ab39c,secondary:#405189"
        style="width:120px;height:120px"></lord-icon>
    <div class="mt-4 pt-2 fs-15">
        <h4>{{ $title }}</h4>
    </div>
</div>
