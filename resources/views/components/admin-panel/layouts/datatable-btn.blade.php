<a href="{{ route($edit_route_name, [$model_name => $id]) }}" class="btn btn-sm btn-soft-info">
    Редактировать
</a>
<button class="btn btn-sm btn-soft-danger show-delete-item-modal" data-bs-toggle="modal" data-bs-target=".delete-modal"
    data-url="{{ route($delete_route_name, [$model_name => $id]) }}">
    Удалить
</button>
