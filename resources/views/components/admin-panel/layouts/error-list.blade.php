@if ($errors->any())
    <div class="alert alert-danger alert-dismissible alert-solid alert-label-icon fade show mb-xl-0" role="alert">
        <i class="ri-error-warning-line label-icon"></i>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
