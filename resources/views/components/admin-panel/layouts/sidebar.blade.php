<div id="scrollbar" translate="no">
    <div class="container-fluid">
        <div id="two-column-menu"></div>
        <ul class="navbar-nav" id="navbar-nav">
{{--            @can('index', \App\Models\Country::class)--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ route('country.index') }}"--}}
{{--                        class="nav-link menu-link {{ request()->routeIs('country.*') ? 'active' : '' }}">--}}
{{--                        <i class="mdi mdi-earth"></i>--}}
{{--                        <span>Страны</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            @endcan--}}
            <li class="nav-item">
                <a href="{{ route('product.index') }}"
                    class="nav-link menu-link {{ request()->routeIs('product.*') ? 'active' : '' }}">
                    <i class="mdi mdi-product"></i>
                    <span>Продукты</span>
                </a>
            </li>

        </ul>
    </div>
</div>
