<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('mail-assets/style.css') }}">
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,500;0,800;1,400;1,500;1,800&display=swap"
        rel="stylesheet">
    <title>Daulet Agabekov</title>
</head>

<body>
    <div class="wrapper">
        <div class="main">
            <img src="{{ asset('mail-assets/Main cover.jpg') }}" alt="Карточка">
        </div>
        <div class="text">
            <p class="start">Дорогой пользователь, изменилась цена на продукт: "{{$title}}"</p>
        </div>
        <div class="apps">
            <div>
                <a href="https://play.google.com/store/apps/details?id=com.gservice&pcampaignid=web_share"
                    target="_blank">
                    <img src="{{ asset('mail-assets/googleplay.svg') }}" alt="Play Market">
                </a>
            </div>
            <div>
                <a href="https://apps.apple.com/kz/app/gservice/id1627674303" target="_blank">
                    <img src="{{ asset('mail-assets/appstore.svg') }}" alt="App Store">
                </a>
            </div>
        </div>
    </div>
</body>

</html>
