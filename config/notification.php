<?php

return [
    'url' => env('NOTIFICATION_URL'),
    'api_key' => env('NOTIFICATION_API_KEY'),
    'enabled' => env('NOTIFICATION_ENABLED'),
    'exception_chat_id' => env('EXCEPTION_CHAT_ID'),
	'push_server_key' => env('PUSH_SERVER_KEY', null),
];
