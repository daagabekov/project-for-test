<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware(['auth:api'])->group(function () {
    Route::get('products', [ProductController::class, 'index']);
    Route::post('products/{product}/follow', [ProductController::class, 'follow']);
    Route::post('products/{product}/unfollow', [ProductController::class, 'unfollow']);
    Route::post('logout', [AuthController::class, 'logout']);
});
