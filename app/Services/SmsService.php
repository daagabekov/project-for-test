<?php

namespace App\Services;

use GuzzleHttp\Client;

class SmsService
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('notification.url'),
        ]);
    }

    public function sendSms($phone, $text, $user_id = null)
    {
        if (!config('notification.enabled')) {
            return false;
        }

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];

        $body = [
            'api_key' => config('notification.api_key'),
            'user_id' => $user_id,
            'phone' => '7' . $phone,
            'text' => $text,
        ];

        try {
            $this->client->request('POST', 'api/v1/sms', [
                'headers' => $headers,
                'body' => json_encode($body, JSON_THROW_ON_ERROR)
            ]);

            return true;
        } catch (\Exception $exception) {
            //
        }

        return false;
    }
}
