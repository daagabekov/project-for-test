<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('price')->get();
        return $this->return_success(ProductResource::collection($products));
    }

    public function follow(Product $product)
    {
        $product->users()->attach([auth()->id()]);
        return $this->return_success();
    }

    public function unfollow(Product $product)
    {
        $product->users()->detach([auth()->id()]);
        return $this->return_success();
    }
}
