<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::create($request->validated());
        $access_token = $user->createToken('auth_token')->accessToken;

        return $this->return_success(['user_token' => $access_token]);
    }

    public function login(LoginRequest $request)
    {
        if ($request->email && Auth::attempt($request->only('email', 'password'))) {
            $user = Auth::user();

            $access_token = $user->createToken('auth_token')->accessToken;

            return $this->return_success(['user_token' => $access_token]);
        } else {
            return $this->return_fail_message(trans('auth.failed'));
        }
    }

    public function logout()
    {
        $user = auth()->user();
        $user->tokens()->delete();

        return $this->return_success();
    }

}
