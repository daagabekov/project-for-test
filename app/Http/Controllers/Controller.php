<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected function return_success($data = null, $additional = []): JsonResponse
    {
        return response()->json(['success' => true, 'data' => $data] + $additional, 200);
    }

    protected function return_fail_message($message = null): JsonResponse
    {
        return response()->json(['success' => false, 'message' => $message], 200);
    }
}
