<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = Product::get();
        return view('admin-panel.product.product-lists', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form_title = 'Добавить продукт';
        $action_url = route('product.store');

        return view('admin-panel.product.product-form', compact('form_title', 'action_url'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductRequest $request)
    {
        $valid_data = $request->validated();
        Product::create($valid_data);

        return redirect()->route('product.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        $form_title = 'Редактировать продукт';
        $action_url = route('product.update', ['product' => $product->id]);

        return view('admin-panel.product.product-form', compact('form_title', 'action_url', 'product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $valid_data = $request->validate([
            'price' => 'required|numeric'
        ]);
        $product->update($valid_data);

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();
    }
}
