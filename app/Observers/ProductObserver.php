<?php

namespace App\Observers;

use App\Mail\PriceChanged;
use App\Models\Product;
use Illuminate\Support\Facades\Mail;

class ProductObserver
{
    public function updated(Product $product)
    {
        if (request()->price) {
            $users = $product->users()->get();
            foreach ($users as $user)
            {
                Mail::to($user->email)->send(new PriceChanged($product->title));
            }
        }
    }
}
